import axios from 'axios'

export function fetchUser() {
    return {
        type: "FETCH_USER_FULFILLED",
        payload: {
            name: 'Jelena',
            age: 18,
            distance: 3.14,
            bio: 'Wild-hearted explorer 📍🌎',
            education: [
                {
                    institution: 'Etelä-Karjalan ammattiopisto'
                },
                {
                    institution: 'Metropolia University of Applied Sciences'
                }
            ],
            work: [
                {
                    company: 'Life',
                    position: 'liver'
                }
            ],
            instagram: null,
            images: [
                'img/5929d69a3b0adde27cd9b1f6/03733213-3eb8-4909-8b9b-3dfa7524e4a6.jpg',
                'img/5929d69a3b0adde27cd9b1f6/22c707a3-edb6-4533-8365-0cb813e89b98.jpg',
                'img/5929d69a3b0adde27cd9b1f6/4ce3a36e-8b12-47fa-b917-4ae6025d2c13.jpg',
                'img/5929d69a3b0adde27cd9b1f6/640d6226-3942-4893-a629-0bc13537bd89.jpg',
                'img/5929d69a3b0adde27cd9b1f6/9b0c89fc-a98f-4818-b25b-aec725cf5790.jpg',
                'img/5929d69a3b0adde27cd9b1f6/9c801c7a-3c84-4400-94b6-78ce7db29b3e.jpg'
            ]
        }
    }
}

export function getUser(id) {
    return {
        type: "GET_USER",
		payload: axios.get("/api/user/"+id)
    }
}

export function getUserList() {
    return {
        type: "GET_USER_LIST",
		payload: axios.get("/api//user_list")
    }
}

export function getUsers() {
    const mockUserList = [
        {tinderId: '5929d69a3b0adde27cd9b1f6', name: 'Jelena', age: 24, distance: 4.8, url: 'img/5929d69a3b0adde27cd9b1f6/03733213-3eb8-4909-8b9b-3dfa7524e4a6.jpg'},
        {tinderId: '575ee9e3940bdf7a0c3ad7d1', name: 'Katri', age: 21, distance: 3.2, url: 'img/575ee9e3940bdf7a0c3ad7d1/526a9b46-cd46-4437-80d0-f93e3567fb57.jpg'},
        {tinderId: '58aca3f81607a2f645b4e3bf', name: 'Ina', age: 19, distance: 1.7, url: 'img/58aca3f81607a2f645b4e3bf/a4fc5782-403d-41bb-8e72-f5722e5ffc90.jpg'},
        {tinderId: '5852313d0273e7f23019268d', name: 'Vivian', age: 22, distance: 3.6, url: 'img/5852313d0273e7f23019268d/2f9325e2-aab9-4b75-85de-6e8e0aec0994.jpg'},
        {tinderId: '585585fb6a5098c4636569b7', name: 'Jaana', age: 21, distance: 2.2, url: 'img/585585fb6a5098c4636569b7/c1b56a30-1cba-4d86-b06b-9e2ed6f84540.jpg'},
        {tinderId: '574c94a1d73ad4f70b576bda', name: 'Heidi', age: 19, distance: 5.3, url: 'img/574c94a1d73ad4f70b576bda/0cdba590-ed86-4b36-8bbc-4f7f0bfdafe4.jpg'},
        {tinderId: '58cda46cdc32c8571ff5f627', name: 'Tea', age: 18, distance: 7.5, url: 'img/58cda46cdc32c8571ff5f627/5b1ed1d8-a543-4afc-84d0-fbda20ae2d69.jpg'},
        {tinderId: '586aa6b9b207a0f213efbe55', name: 'Elina', age: 22, distance: 3.9, url: 'img/586aa6b9b207a0f213efbe55/34f24b74-7af4-49f8-878d-2150dc84aa1e.jpg'},
        {tinderId: '587d077fb8cf484d4effe790', name: 'Emma', age: 19, distance: 4.7, url: 'img/587d077fb8cf484d4effe790/fffba447-4d89-4d2c-a47e-23da69c751d0.jpg'},
        {tinderId: '55b68240e3eaea393cebdb90', name: 'Sandra-Kristina', age: 20, distance: 2.0, url: 'img/55b68240e3eaea393cebdb90/7a8ad229-394d-4408-8d54-de1327974c01.jpg'},
        {tinderId: '5360433fe3f34f804300b859', name: 'Julia', age: 19, distance: 0.5, url: 'img/5360433fe3f34f804300b859/afbab77f-1db4-4e80-b7ab-972ea8968630.jpg'},
        {tinderId: '5809251f2a01ad647bbe1bf9', name: 'Kaisa', age: 18, distance: 1.0, url: 'img/5809251f2a01ad647bbe1bf9/59e553f4-8e56-44ab-94e5-340b05a11884.jpg'},
        {tinderId: '560132ef0f98612213e94d01', name: 'Tilda', age: 18, distance: 2.4, url: 'img/560132ef0f98612213e94d01/0b04571d-b654-4634-808c-d800450a1d53.jpg'}
    ]
    return {
        type: "GET_USERS_FULFILLED",
        payload: mockUserList
    }
}


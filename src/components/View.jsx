import React from 'react';
import ReactDOM from 'react-dom';

import styles from './scss/view.scss'

export default class View extends React.Component {
    render() {
        return (
            <div className='view fill'>
                {this.props.children}
            </div>
        )
    }
}

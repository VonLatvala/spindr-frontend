import React from 'react';
import ReactDOM from 'react-dom';

import { Row, Col } from 'react-bootstrap'

import View from '../components/View.jsx'
import ViewTitle from '../components/ViewTitle.jsx'
import UserList from './UserList.jsx'
import UserDisplay from './UserDisplay.jsx'

export default class UserExplorerView extends React.Component {
    render() {
        return (
            <View>
                <ViewTitle text="User Explorer"></ViewTitle>
                <Row className='user-explorer-container fill nonMobilePadder'>
                    <Col xs={12} sm={4} className="fill">
                        <UserList/>
                    </Col>
                    <Col xs={12} sm={8} className="fill">
                        <UserDisplay/>
                    </Col>
                </Row>
            </View>
        )
    }
}

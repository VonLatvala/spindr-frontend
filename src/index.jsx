import { Provider } from 'react-redux'

import React from 'react'
import ReactDOM from 'react-dom'
import Root from './containers/Root.jsx'
import store from './store.jsx'

ReactDOM.render(
    Root({store})
    , document.getElementById('app')
);

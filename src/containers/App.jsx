import React from 'react'
import ReactDOM from 'react-dom'


import MainView from '../components/MainView.jsx'
import AppNav from '../components/AppNav.jsx'
import UserExplorerView from './UserExplorerView.jsx'

import Bootstrap from 'bootstrap/dist/css/bootstrap.css'


export default class App extends React.Component {
    render() {
        return (
            <div className='fill'>
                <AppNav appName="SpindR" />
                <UserExplorerView />
            </div>
        );
    }
}

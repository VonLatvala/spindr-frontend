import { combineReducers } from "redux"

import userService from './userReducer.jsx'
import userIdListService from './userListReducer.jsx'

export default combineReducers({
    userService,
    userIdListService,
})

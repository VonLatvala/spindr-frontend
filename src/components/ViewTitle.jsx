import React from 'react';
import ReactDOM from 'react-dom';

export default class ViewTitle extends React.Component {
    render() {
        return (
            <h1 className='hidden-xs' style={{marginTop: 0, marginBottom: '20px'}}>{this.props.text}</h1>
        )
    }
}

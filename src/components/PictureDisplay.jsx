import React from 'react';
import ReactDOM from 'react-dom';
import { Row } from 'react-bootstrap';

import styles from './scss/PictureDisplay.scss'

import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Slider from 'react-slick';

import PictureElement from './PictureElement.jsx';

export default class PictureDisplay extends React.Component {
    render() {
        let settings = {
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: false,
            centerMode: true,
            arrows: true
        }
        let pictureElements = []
        if (this.props.urls.lenght > 0)
            pictureElements = [(<div key='NOITEM'><h3>Nobody selected<img /></h3></div>)]
        else
            pictureElements = this.props.urls.map((val, idx) => {
                return (
                    <div key={val}>
                        <h3>
                            <img style={{width: '100%', margin: '0 auto'}} src={val} />
                        </h3>
                    </div>
                )
            })
        console.log(pictureElements)
        return (
            <div style={{paddingBottom: '40px', paddingLeft: '10px', paddingRight: '10px'}}>
                <Slider {...settings}>
                    {pictureElements}
                </Slider>
            </div>
        );
    };
}

import React from 'react';
import ReactDOM from 'react-dom';

export default class PictureElement extends React.Component {
    render() {
        return (
            <div>
                <h3>
                    <img style={{height: '300px', margin: '0 auto'}} src={this.props.url} alt={this.props.title} />
                </h3>
            </div>
        );
    };
}

import React from 'react'
import PropTypes from 'react-proptypes'
import { Provider } from 'react-redux'
import App from './App.jsx'

const Root = ({store}) => (
    <Provider store={store}>
        <App name="SpindR" />
    </Provider>
)

export default Root

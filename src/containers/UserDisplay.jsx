import React from 'react';
import ReactDOM from 'react-dom';

import { connect } from 'react-redux'
import { getUser, getUserList } from '../actions/userActions.jsx'

import { Table, Panel, Button } from 'react-bootstrap';

import PictureDisplay from '../components/PictureDisplay.jsx';

export default class UserDisplay extends React.Component {
    render() {
        let userObj = this.props.user || {id: 1, tinder_id: "NOUSER", name: "NOUSER", birth_date_timestamp: 0, distance_mi: 0, discovery_long: -1.0, discovery_lat: -1.0, bio: "No user selected yet", instagram_username: null, photos: [], instagram_photos: [], jobs: [], educations: [], votes: []}
        let educationRows = userObj.educations.map((val, idx) =>
            <li key={val.institution}>
                {val.institution}
            </li>
        )
        let workRows = userObj.jobs.map((val, idx) =>
            <li key={val.position + val.company}>
                {val.position} at {val.company}
            </li>
        )
        let panelHeader = (
            <span>
                <span className='hidden-sm hidden-md hidden-lg'><Button>Show userlist</Button></span>
                <strong>{userObj.name}, {userObj.age}</strong>
            </span>
        )
        return (
            <Panel header={panelHeader} className="fill" style={{overflowY: 'auto'}}>
                <PictureDisplay urls={userObj.photos.map((elem) => {'img/'+userObj.tinder_id+'/'+elem.tinder_photo_id+'.jpg'})} />
                <div>
                    <Table responsive>
                        <tbody>
                            <tr>
                                <th>
                                    Distance
                                </th>
                                <td>
                                    {userObj.distance}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Education
                                </th>
                                <td>
                                    <ul>
                                        {educationRows}
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Work
                                </th>
                                <td>
                                    <ul>
                                        {workRows}
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Instagram
                                </th>
                                <td>
                                    {userObj.instagram === null ? "-" : (<a href="https://instagr.am/{userObj.instagram}">{userObj.instagram}</a>)}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Bio
                                </th>
                                <td>
                                    {userObj.bio}
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </Panel>
        );
    };
}

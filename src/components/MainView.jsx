import React from 'react'
import ReactDOM from 'react-dom'

import View from './View.jsx'
import ViewTitle from './ViewTitle.jsx'
import UserExplorerView from '../containers/UserExplorerView.jsx'

export default class MainView extends React.Component {
    render() {
        return (
            <View>
                <ViewTitle text="Main"></ViewTitle>
            </View>
        )
    }
}

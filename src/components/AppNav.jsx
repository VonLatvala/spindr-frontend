import React from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Nav, NavItem, Brand } from 'react-bootstrap';

import styles from './AppNav.scss'

export default class AppNavbar extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Navbar fixedTop={true} inverse={true}>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">{this.props.appName}</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem>
                            Pizdec
                        </NavItem>
                        <NavItem>
                            Blet
                        </NavItem>
                        <NavItem>
                            Haxyñ
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    };
}

import React from 'react';
import ReactDOM from 'react-dom';
import { Col, Nav, NavItem, FormControl } from 'react-bootstrap';

import styles from './Sidebar.scss'

export default class Sidebar extends React.Component {
    render() {
        return (
            <Col sm={3} md={2} className="sidebar">
                <Nav bsStyle="pills" stacked={true}>
                    <NavItem active={true}>Galleria</NavItem>
                    <NavItem>Matches</NavItem>
                    <NavItem><FormControl type="input" /></NavItem>
                </Nav>
            </Col>
        );
    };
}

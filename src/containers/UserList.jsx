import React from 'react'
import ReactDOM from 'react-dom'

import { connect } from 'react-redux'

import { getUsers, getUserList } from '../actions/userActions.jsx'

import { Panel, InputGroup, FormControl, Button, Glyphicon, ListGroup, ListGroupItem } from 'react-bootstrap'

@connect((store) => {
    return {
        userIds: store.userIdListService.data,
        users: store.userService.data
    }
})
export default class UserList extends React.Component {
    componentWillMount() {
        //this.props.dispatch(getUserList())
    }
    render() {
        let onClickPlaceholder = () => {}
        const listItemGenerator = (tinder_id, name, birth_date_timestamp, photos, distance_mi) => {
            return (
                <button className="list-group-item" key={tinder_id}>
                    <div className="col-xs-4 fill">
                        <img src={"img/"+tinder_id+"/"+photos[0].tinder_photo_id+".jpg"} style={{width: '100%'}} />
                    </div>
                    <div className="col-xs-8">
                        <h4>
                            {name}
                        </h4>
                        <p className="list-group-item-text">
                            Age: {parseInt((Date.now()*1e-3-birth_date_timestamp)/(60*60*24*365))}, {distance_mi*1.609344}km away
                        </p>
                    </div>
                </button>
            )
        }
        const loadingElem = (
            <button className="list-group-item" key="loadingelem">
                <div className="col-xs-12">
                    <h4>
                        Loading user list
                    </h4>
                    <p className="list-group-item-text">
                        Please wait
                    </p>
                </div>
            </button>
        )
        const userElements = this.props.users.length ? this.props.users.map((elem) => listItemGenerator(elem.tinder_id, elem.name, elem.birth_date_timestamp, elem.photos, elem.distance_mi)): [loadingElem]
        let panelHeader = (
            <InputGroup>
                <FormControl placeholder="Filter" />
                <InputGroup.Button>
                    <Button>
                        <Glyphicon glyph='remove-circle' />
                    </Button>
                </InputGroup.Button>
            </InputGroup>
        )
        return (
            <Panel className='fill no-padding-panel-body' header={panelHeader}>
                <div className='fill' style={{paddingBottom: '55px'}}>
                    <ListGroup className='fill' style={{overflowY: 'auto'}}>
                        {userElements}
                    </ListGroup>
                </div>
            </Panel>
        )
    }
}

import React from 'react'
import ReactDOM from 'react-dom'

import { connect } from 'react-redux'

import { Col } from 'react-bootstrap'

import UserDisplay from './UserDisplay.jsx'

@connect((store) =>{
    selectedUser: null
})
export default class Dashboard extends React.Component {
    render() {
        return (
            <Col sm={9} smOffset={3} md={10} mdOffset={2} className="dashboard">
                <h1 className="page-header">Dashboard</h1>
                <UserDisplay user={this.props.user}/>
            </Col>
        );
    };
}

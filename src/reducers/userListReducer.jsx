export default function reducer(state = {
    data: [],
    fetching: false,
    fetched: false,
    error: null,
}, action) {
    switch(action.type) {
        case 'GET_USER_LIST_PENDING': {
            return {...state, fetching: true}
            break;
        }
        case 'GET_USER_LIST_REJECTED': {
            return {...state, fetching: false, error: action.payload}
            break;
        }
        case 'GET_USER_LIST_FULFILLED': {
            return {...state, fetching: false, fetched: true, data: action.payload}
            break;
        }
    }
    return state
}
